//
//  TrackDetailsVO.m
//  Rollersway
//
//  Created by Artur Balabanskyy on 7/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TrackDetailsVO.h"


@implementation TrackDetailsVO
@synthesize distance;
@synthesize currentSpeed;
@synthesize averageSpeed;
@synthesize totalTime;
@synthesize currentAltitude;
@synthesize maxSpeed;
@synthesize calories;
@synthesize activityTypeDetails;
@synthesize activityType;

- (id)init {
    self = [super init];
    if (self) {
        distance = 0;
        currentSpeed = 0;
        averageSpeed = 0;
        totalTime = 0;
        currentAltitude = 0;
        maxSpeed = 0;
        calories = 0;
    }
    return self;
}

-(void)setDistance:(double)value
{
    if (isnan(value)) {
        distance = 0;
    }
    else
    {
        distance = value < 0 ? 0 : value;
    }
}

-(void)setTotalTime:(double)value
{
    if(isnan(value)){
        totalTime = 0;
    }
    else
    {
        totalTime = value < 0 ? 0 : value;
    }
}

-(void)setAverageSpeed:(double)value
{
    if(isnan(value)){
        averageSpeed = 0;
    }
    else
    {
        averageSpeed = value < 0 ? 0 : value;
    }
}



-(void)setCurrentSpeed:(double)value
{
    if(isnan(value)){
        currentSpeed = 0;
    }
    else
    {
        currentSpeed = value < 0 ? 0 : value;
    }
}


-(void)setMaxSpeed:(double)value
{
    if(isnan(value)){
        maxSpeed = 0;
    }
    else
    {
        maxSpeed = value < 0 ? 0 : value;
    }
}

-(void)setCalories:(double)value
{
    if(isnan(value)){
        calories = 0;
    }
    else
    {
        calories = value < 0 ? 0 : value;
    }
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
	[aCoder encodeDouble:self.distance forKey:@"distance"];
	[aCoder encodeDouble:self.currentSpeed forKey:@"currentSpeed"];
	[aCoder encodeDouble:self.averageSpeed forKey:@"averageSpeed"];
    [aCoder encodeDouble:self.totalTime forKey:@"totalTime"];
    [aCoder encodeDouble:self.maxSpeed forKey:@"maxSpeed"];
    [aCoder encodeDouble:self.currentAltitude forKey:@"currentAltitude"];
    [aCoder encodeDouble:self.calories forKey:@"calories"];
    [aCoder encodeObject:self.activityType forKey:@"activityType"];
    [aCoder encodeObject:self.activityTypeDetails forKey:@"activityTypeDetails"];
}

-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
	if(self){
		self.distance = [aDecoder decodeDoubleForKey:@"distance"];
		self.currentSpeed = [aDecoder decodeDoubleForKey:@"currentSpeed"];
		self.averageSpeed = [aDecoder decodeDoubleForKey:@"averageSpeed"];
		self.totalTime = [aDecoder decodeDoubleForKey:@"totalTime"];
        self.currentAltitude = [aDecoder decodeDoubleForKey:@"currentAltitude"];
        self.maxSpeed = [aDecoder decodeDoubleForKey:@"maxSpeed"];
        self.calories = [aDecoder decodeDoubleForKey:@"calories"];
        self.activityType = [aDecoder decodeObjectForKey:@"activityType"];
        self.activityTypeDetails = [aDecoder decodeObjectForKey:@"activityTypeDetails"];
	}
    return self;
}

@end
