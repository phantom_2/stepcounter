//
//  UserLocationTracker.h
//  MapTestApplication
//
//  Created by Artur Balabanskyy on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "UserTrackerDelegate.h"
#import "TrackDetailsVO.h"
#import <CoreMotion/CoreMotion.h>

@interface UserLocationTracker : NSObject <CLLocationManagerDelegate> {
}

@property (nonatomic, retain) CMMotionManager* motionManager;
@property (nonatomic, retain) NSString* logString;

-(void) startUpdatingLocation;
-(void) stop;
@end
