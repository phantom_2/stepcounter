//
//  UserTrackerDelegate.h
//  MapTestApplication
//
//  Created by Artur Balabanskyy on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TrackDetailsVO.h"

@protocol UserTrackerDelegate <NSObject>

-(void) updateTrack:(NSArray*) coordinates;
-(void) updateLocation:(CLLocation*) location;
-(void) updateDetails:(TrackDetailsVO*) details;

@optional
-(void) updateLog:(NSString*) logString;
@end
