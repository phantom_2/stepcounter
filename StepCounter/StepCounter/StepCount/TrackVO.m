//
//  TrackVO.m
//  Rollersway
//
//  Created by Artur Balabanskyy on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TrackVO.h"
#import "ParseUtils.h"

@implementation TrackVO
@synthesize trackId;
@synthesize userId;
@synthesize username;
@synthesize trackName;
@synthesize lat;
@synthesize lon;
@synthesize types;
@synthesize pointsDesc;
@synthesize trackInfo;
@synthesize createDate;
@synthesize trackTitle;
@synthesize trackSource;
@synthesize screenShort;
@synthesize center;
@synthesize address;
@synthesize views;
@synthesize avgSpeed;
@synthesize maxSpeed;
@synthesize distance;
@synthesize distanceTime;

- (id)initWithDictionary:(NSDictionary*) dict {
    self = [super init];
    if (self) {
        NSNumberFormatter * f = [[[NSNumberFormatter alloc] init] autorelease];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        self.trackId = [dict objectForKey:@"id"];
        self.userId = [dict objectForKey:@"UID"];
        self.username = [dict objectForKey:@"username"];
        self.trackName = [dict objectForKey:@"track_name"];
        self.lat = [ParseUtils parseSemicolonSeparatedValuesToArray:[dict objectForKey:@"lat"]];
        self.lon = [ParseUtils parseSemicolonSeparatedValuesToArray:[dict objectForKey:@"lon"]];
        self.types = [ParseUtils parseSemicolonSeparatedValuesToArray:[dict objectForKey:@"types"]];
        self.pointsDesc = [ParseUtils parseSemicolonSeparatedValuesToArray:[dict objectForKey:@"points_desc"]];
        self.trackInfo = [dict objectForKey:@"track_info"];
        self.createDate = [dict objectForKey:@"create_date"];
        self.trackTitle = [dict objectForKey:@"track_title"];
        self.trackSource = [dict objectForKey:@"track_source"];
        self.screenShort = [dict objectForKey:@"screen_short"];
        self.center = [ParseUtils parseSemicolonSeparatedValuesToCoordinate:[dict objectForKey:@"track_source"]];
        self.address = [dict objectForKey:@"address"];
        self.views = [dict objectForKey:@"views"];
        self.avgSpeed = [f numberFromString:[dict objectForKey:@"avg_speed"]];
        if (self.avgSpeed == nil) {
            self.avgSpeed = [NSNumber numberWithInt:0];
        }
        self.maxSpeed = [f numberFromString:[dict objectForKey:@"max_speed"]];
        if (self.maxSpeed == nil) {
            self.maxSpeed = [NSNumber numberWithInt:0];
        }
        self.distance = [f numberFromString:[dict objectForKey:@"distance"]];
        if (self.distance == nil) {
            self.distance = [NSNumber numberWithInt:0];
        }
        //Convert to meters
        self.distance = [NSNumber numberWithDouble:([self.distance doubleValue]*1000)];
        self.distanceTime = [f numberFromString:[dict objectForKey:@"distance_time"]];
        if (self.distanceTime == nil) {
            self.distanceTime = [NSNumber numberWithInt:0];
        }
    }
    return self;
}

- (void)dealloc {
    [trackId release];
    [userId release];
    [username release];
    [trackName release];
    [lat release];
    [lon release];
    [types release];
    [pointsDesc release];
    [trackInfo release];
    [createDate release];
    [trackTitle release];
    [trackSource release];
    [screenShort release];
    [address release];
    [views release];
    [avgSpeed release];
    [maxSpeed release];
    [distance release];
    [distanceTime release];
    [super dealloc];
}
@end
