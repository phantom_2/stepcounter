//
//  UserLocationTracker.m
//  MapTestApplication
//
//  Created by Artur Balabanskyy on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define kCaloriesCalculatePeriod 10

#define kDistanceFilterValue 10
#define kAutoPauseSeconds 10


#import "UserLocationTracker.h"

#import "DDLogMacros.h"
#import "VectorVO.h"
#import <UIKit/UIKit.h>

#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_WARN;
#endif

@interface UserLocationTracker ()
{
    float calculatedSpeedFromPreOldLocationToOldLocation2;
    float calculatedSpeedFromPreOldLocationToOldLocation3;
    
    VectorVO* _currentAcceleration;
    VectorVO* _previousAcceleration;
    
    VectorVO* _currentHorizontalAcceleration;
    VectorVO* _previousHorizontalAcceleration;
    
    NSMutableArray* _scalarProductsArray;
    NSMutableArray* _anglesArray;
    NSMutableArray* _horizontalScalarProductsArray;
    NSMutableArray* _horizontalAnglesArray;
    
    NSMutableArray* _accelerationArray;

    
    int steps1;
    int steps2;
    int steps3;
    int steps4;
    int counter;
    
    NSDate* _currentTime;
    NSDate* _previousTime;
}

@end

@implementation UserLocationTracker
@synthesize logString;
@synthesize motionManager;

-(id) init{
	if((self = [super init])){
        
        _scalarProductsArray = [NSMutableArray array];
        _horizontalScalarProductsArray = [NSMutableArray array];
        _anglesArray = [NSMutableArray array];
        _accelerationArray = [NSMutableArray array];
        
        steps1 = 0;
        steps2 = 0;
        steps3 = 0;
        steps4 = 0;
        
//        [self startUpdatingLocation];
	}
	return self;
}


-(void) startUpdatingLocation{
    

        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.showsDeviceMovementDisplay = YES;
        self.motionManager.deviceMotionUpdateInterval = 0.1;
        

        CMDeviceMotionHandler  motionHandler = ^ (CMDeviceMotion *motion, NSError *error) {
            
            
            double AX = motion.userAcceleration.x;
            double AY = motion.userAcceleration.y;
            double AZ = motion.userAcceleration.z;
            double GX = motion.gravity.x;
            double GY = motion.gravity.y;
            double GZ = motion.gravity.z;
            
            _previousAcceleration = _currentAcceleration;
            _currentAcceleration = [[VectorVO alloc] init];
            _currentAcceleration.x = AX;
            _currentAcceleration.x = AY;
            _currentAcceleration.x = AZ;
            
            
            double func = (GX*AX + GY * AY + GZ*AZ)/( pow(GX,2) + pow(GY,2) + pow(GZ,2) );
            double x = GX * func;
            double y = GY * func;
            double z = GZ * func;
            
            _previousHorizontalAcceleration = _currentHorizontalAcceleration;
            _currentHorizontalAcceleration = [[VectorVO alloc] init];
            _currentHorizontalAcceleration.x = x;
            _currentHorizontalAcceleration.x = y;
            _currentHorizontalAcceleration.x = z;
            
            
            double acceleration2 = sqrt (pow(AX-x,2) + pow(AY-y,2) + pow(AZ-z,2));
            
            
            [_accelerationArray addObject:@(acceleration2)];
            
            
//            if (maxAcc < acceleration2) {
//                maxAcc = acceleration2;
//            }
            
            
            double scalarValue = 0;
            double anglesValue = 0;
            
            double horisontalScalarValue = 0;
            double horisontalAnglesValue = 0;
            
            if (_currentAcceleration != _previousAcceleration) {
                
                
                scalarValue = [self scalarProduct:_currentAcceleration currentVector:_previousAcceleration];
                [_scalarProductsArray addObject:[NSNumber numberWithFloat:scalarValue]];
                
                anglesValue = [self angle:_currentAcceleration currentVector:_previousAcceleration];
                if (_anglesArray.count > 0) {
                    if (anglesValue > 0 && [[_anglesArray lastObject] floatValue] < 0) {
                        counter++;
                    }
                }
                [_anglesArray addObject:[NSNumber numberWithFloat:anglesValue]];
                
                
//                if (_scalarProductsArray.count > 2) {
//                 
//                    if (([[_scalarProductsArray objectAtIndex:_scalarProductsArray.count - 2] floatValue]- [[_scalarProductsArray lastObject] floatValue] > 1) && ([[_scalarProductsArray objectAtIndex:_scalarProductsArray.count - 2] floatValue] * [[_scalarProductsArray lastObject] floatValue] < 0)) {
//                        
//                    }
//                }
                if ([self _isStep:_scalarProductsArray]) {
                    steps1 = steps1 + 1;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"s1" object:[NSNumber numberWithFloat:steps1]];
                }
                if ([self _isAngleStep:_anglesArray]) {
                    steps2 = steps2 + 1;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"s2" object:[NSNumber numberWithFloat:steps2]];
                }

                
                
                horisontalScalarValue = [self scalarProduct:_currentHorizontalAcceleration currentVector:_previousHorizontalAcceleration];
                
                horisontalAnglesValue = [self angle:_currentHorizontalAcceleration currentVector:_previousHorizontalAcceleration];
                
                [_horizontalScalarProductsArray addObject:[NSNumber numberWithFloat:horisontalScalarValue]];
                
                [_horizontalAnglesArray addObject:[NSNumber numberWithFloat:horisontalAnglesValue]];
                
                if ([self _isStep:_horizontalScalarProductsArray]) {
                    steps3 = steps3 + 1;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"s3" object:[NSNumber numberWithFloat:steps3]];
                }
                if ([self _isAngleStep:_horizontalAnglesArray]) {
                    steps4 = steps4 + 1;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"s4" object:[NSNumber numberWithFloat:steps4]];
                }
                
            }
            
            
            _previousTime = _currentTime;
            _currentTime = [NSDate new];
            float time = [_currentTime timeIntervalSinceDate:_previousTime];
            
            
            NSString* str = [NSString stringWithFormat:@"%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%i;%i;%i;%i;%f;%f;%f;%f;%d\n",
                             time,
                             AX,
                             AY,
                             AZ,
                             GX,
                             GY,
                             GZ,
                             func,
                             x,
                             y,
                             z,
                             acceleration2,
                             steps1,
                             steps2,
                             steps3,
                             steps4,
                             scalarValue,
                             anglesValue,
                             horisontalScalarValue,
                             horisontalAnglesValue,
                             counter];
            
            self.logString = [self.logString stringByAppendingString:str];
//            DDLogError(@"%@",str);
            

        };
    
        [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXTrueNorthZVertical toQueue:[NSOperationQueue currentQueue] withHandler:motionHandler];
    
        
    
}

-(BOOL)_isStep:(NSMutableArray*)array
{
    if (array.count > 2) {
        
        if (([[array objectAtIndex:array.count - 2] floatValue]- [[array lastObject] floatValue] > 1) && ([[array objectAtIndex:array.count - 2] floatValue] * [[array lastObject] floatValue] < 0)) {
            return YES;
        }
    }

    return NO;
}

-(BOOL)_isAngleStep:(NSMutableArray*)array
{
    if (array.count > 2) {
        
        if (([[array objectAtIndex:array.count - 2] floatValue]- [[array lastObject] floatValue] > 0.3) && ([[array objectAtIndex:array.count - 2] floatValue] * [[array lastObject] floatValue] < 0)) {
            return YES;
        }
    }
    
    return NO;
}


-(float)scalarProduct:(VectorVO*)vector1 currentVector:(VectorVO*)vector2
{
    
    float product = vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z;
    
    return product;
}

-(float)angle:(VectorVO*)vector1 currentVector:(VectorVO*)vector2
{
    float vector1Abs = sqrt (pow(vector1.x,2) + pow(vector1.y,2) + pow(vector1.z,2));
    float vector2Abs = sqrt (pow(vector2.x,2) + pow(vector2.y,2) + pow(vector2.z,2));

    float angle = [self scalarProduct:vector1 currentVector:vector2] / ( vector1Abs * vector2Abs);
    
    return angle;
}

-(void) stop
{
    [motionManager stopDeviceMotionUpdates];
}


@end
