//
//  TrackVO.h
//  Rollersway
//
//  Created by Artur Balabanskyy on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TrackVO : NSObject {
    NSString* trackId;
    NSString* userId;
    NSString* username;
    NSString* trackName;
    NSArray* lat;
    NSArray* lon;
    NSArray* types;
    NSArray* pointsDesc;
    NSString* trackInfo;
    NSString* createDate;
    NSString* trackTitle;
    NSString* trackSource;
    NSString* screenShort;
    CLLocationCoordinate2D center;
    NSString* address;
    NSString* views;
    NSNumber* avgSpeed;
    NSNumber* maxSpeed;
    NSNumber* distance;
    NSNumber* distanceTime;
    

}

@property (nonatomic,retain) NSString* trackId;
@property (nonatomic,retain) NSString* userId;
@property (nonatomic,retain) NSString* username;
@property (nonatomic,retain) NSString* trackName;
@property (nonatomic,retain) NSArray* lat;
@property (nonatomic,retain) NSArray* lon;
@property (nonatomic,retain) NSArray* types;
@property (nonatomic,retain) NSArray* pointsDesc;
@property (nonatomic,retain) NSString* trackInfo;
@property (nonatomic,retain) NSString* createDate;
@property (nonatomic,retain) NSString* trackTitle;
@property (nonatomic,retain) NSString* trackSource;
@property (nonatomic,retain) NSString* screenShort;
@property (nonatomic,retain) NSString* address;
@property (nonatomic,retain) NSString* views;
@property (nonatomic,assign) CLLocationCoordinate2D center;
@property (nonatomic,retain) NSNumber* avgSpeed;
@property (nonatomic,retain) NSNumber* maxSpeed;
@property (nonatomic,retain) NSNumber* distance;
@property (nonatomic,retain) NSNumber* distanceTime;

- (id)initWithDictionary:(NSDictionary*) dict;
@end
