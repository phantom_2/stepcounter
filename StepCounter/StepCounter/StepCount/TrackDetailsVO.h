//
//  TrackDetailsVO.h
//  Rollersway
//
//  Created by Artur Balabanskyy on 7/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TrackDetailsVO : NSObject {
    double averageSpeed;
    double currentSpeed;
    double distance;
    double totalTime;
    double currentAltitude;
    double maxSpeed;
    double calories;
    NSString* activityType;
    NSString* activityTypeDetails;
    
}
@property (nonatomic, assign) double averageSpeed;
@property (nonatomic, assign) double currentSpeed;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) double totalTime;
@property (nonatomic, assign) double currentAltitude;
@property (nonatomic, assign) double maxSpeed;
@property (nonatomic, assign) double calories;
@property (nonatomic, retain) NSString* activityType;
@property (nonatomic, retain) NSString* activityTypeDetails;
@end
