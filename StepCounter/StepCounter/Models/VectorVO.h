//
//  VectorVO.h
//  StepCounter
//
//  Created by Ivan Chuchman on 10/9/14.
//  Copyright (c) 2014 SC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VectorVO : NSObject

@property (nonatomic, assign) float x;
@property (nonatomic, assign) float y;
@property (nonatomic, assign) float z;

@end
