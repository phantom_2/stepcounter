//
//  ViewController.h
//  StepCounter
//
//  Created by Ivan Chuchman on 8/6/14.
//  Copyright (c) 2014 SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(void) updateLog:(NSString*) logString;

@end

