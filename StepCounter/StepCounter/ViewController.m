//
//  ViewController.m
//  StepCounter
//
//  Created by Ivan Chuchman on 8/6/14.
//  Copyright (c) 2014 SC. All rights reserved.
//

#import "DDLogMacros.h"

#import "ViewController.h"
#import <AudioToolbox/AudioServices.h>

#import "UserLocationTracker.h"

#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_WARN;
#endif
@interface ViewController (){

    __weak IBOutlet UITextView *_logTextView;
    UserLocationTracker* _lt;
    __weak IBOutlet UILabel *_s1Label;
    __weak IBOutlet UILabel *_s2Label;
    __weak IBOutlet UILabel *_s3Label;
    __weak IBOutlet UILabel *_s4Label;
}
            

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _lt = [[UserLocationTracker alloc] init];
    
//    [NSTimer scheduledTimerWithTimeInterval:0.01 target:lt selector:@selector(startUpdatingLocation) userInfo:nil repeats:YES];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_s1:)
                                                 name:@"s1"
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_s2:)
                                                 name:@"s2"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_s3:)
                                                 name:@"s3"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_s4:)
                                                 name:@"s4"
                                               object:nil];
}

-(void)_s1:(NSNotification*)sender
{
    NSNumber* number = sender.object;
    _s1Label.text = [NSString stringWithFormat:@"%@", number];

}

-(void)_s2:(NSNotification*)sender
{
    
    NSNumber* number = sender.object;
    _s2Label.text = [NSString stringWithFormat:@"%@", number];

}

-(void)_s3:(NSNotification*)sender
{
    
    NSNumber* number = sender.object;
    _s3Label.text = [NSString stringWithFormat:@"%@", number];

}

-(void)_s4:(NSNotification*)sender
{
    NSNumber* number = sender.object;
    _s4Label.text = [NSString stringWithFormat:@"%@", number];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateLog:(NSString*) logString{
    _logTextView.text = [NSString stringWithFormat:@"%@\n/////---////\n%@", _logTextView.text,logString];
}


-(IBAction) _actionButtonPressed:(UIButton*)button
{
    DDLogError(@"%@",button.titleLabel.text);
    _lt.logString = @"";
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        
        [_lt startUpdatingLocation];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(20 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
            [_lt stop];
            DDLogError(@"%@",_lt.logString);
        });
    });
}




@end
